# Rappi Business Case
## José Jaime Méndez Flores

Los documentos pdf en la carpeta principal tienen los reportes correspondientes a la parte de análisis de datos (Questions_1_2) y al modelo de detección de fraude (Questions_3_to_end).  
  
Los códigos en  R (para la parte de análisis) y Python (para la parte del modelo) están en la carpeta Code. Se usaron las versiones 4.1 de R y 3.8 de Python.  
  
Los datos primarios y secundarios se encuentran en la carpeta Data.

 